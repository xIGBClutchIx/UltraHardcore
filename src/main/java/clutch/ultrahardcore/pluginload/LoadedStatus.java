package clutch.ultrahardcore.pluginload;

import clutch.ultrahardcore.misc.Constants;
import org.bukkit.ChatColor;

public enum LoadedStatus {

    STARTING(Constants.INFO_COLOR),
    INITIATING(Constants.FINE_COLOR),
    LOADED(Constants.DONE_COLOR);

    private ChatColor color;

    LoadedStatus(ChatColor color) {
        this.color = color;
    }

    public ChatColor getColor() {
        return color;
    }
}
