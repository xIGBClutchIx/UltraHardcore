package clutch.ultrahardcore.pluginload;

import clutch.ultrahardcore.lang.LanguageManager;
import clutch.ultrahardcore.misc.Constants;
import clutch.ultrahardcore.misc.UHCUtils;
import clutch.ultrahardcore.pluginload.biome.BiomeReplacer;
import org.bukkit.ChatColor;

public class PluginLoadManger {

    private static int loadedLoopTask;
    private static LoadedStatus loadedStatus = null;

    public static void init() {
        // Check if loaded
        if (loadedStatus != null) return;
        // Starting
        setLoadedStatus(LoadedStatus.STARTING);
        // Biome replacement init
        BiomeReplacer.init();
        // Login event
        UHCUtils.registerListener(new PluginLoadListener());
        // Schedule loop
        loadedLoopTask = UHCUtils.runTaskTimer(new WorldCheckRunnable(), 0, 3).getTaskId();
    }

    public static void loaded() {
        // Check if loaded
        if (loadedStatus == LoadedStatus.LOADED) return;
        // End loop
        UHCUtils.cancelTask(loadedLoopTask);
        // Done loading
        setLoadedStatus(LoadedStatus.LOADED);
    }

    public static void setLoadedStatus(LoadedStatus status) {
        // Check if loaded or null
        if (loadedStatus == LoadedStatus.LOADED || status == null) return;
        LanguageManager.printColor(status.getColor(), "Load Status: " + status.name());
        loadedStatus = status;
    }

    private static LoadedStatus getLoadedStatus() {
        return loadedStatus;
    }

    public static boolean isStarting() {
        return PluginLoadManger.getLoadedStatus() == LoadedStatus.STARTING;
    }

    public static boolean isLoaded() {
        return PluginLoadManger.getLoadedStatus() == LoadedStatus.LOADED;
    }
}
