package clutch.ultrahardcore.pluginload;

import clutch.ultrahardcore.misc.Constants;
import clutch.ultrahardcore.lang.LanguageManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class PluginLoadListener implements Listener {

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent event) {
        if (PluginLoadManger.isLoaded()) return;
        event.disallow(PlayerLoginEvent.Result.KICK_OTHER,
                LanguageManager.fullNameFormat(Constants.ERROR_COLOR + "Server still loading!"));
    }
}
