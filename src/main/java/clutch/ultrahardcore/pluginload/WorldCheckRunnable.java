package clutch.ultrahardcore.pluginload;

import clutch.ultrahardcore.misc.UHCUtils;

public class WorldCheckRunnable implements Runnable {

    @Override
    public void run() {
        if (PluginLoadManger.isStarting() && UHCUtils.getNumberOfWorlds() > 1) {
            PluginLoadManger.setLoadedStatus(LoadedStatus.INITIATING);
            UltraHardcorePlugin.onInitialize();
        }
    }
}
