package clutch.ultrahardcore.pluginload;

import clutch.ultrahardcore.UltraHardcore;
import org.bukkit.plugin.java.JavaPlugin;

public class UltraHardcorePlugin extends JavaPlugin {

    /* Instance */
    private static UltraHardcorePlugin instance;

    /** Loads before world generation */
    @Override
    public void onEnable() {
        // Instance
        instance = this;
        // Init plugin load handler
        PluginLoadManger.init();
        UltraHardcore.onEnable();
    }

    @Override
    public void onDisable() {
        UltraHardcore.onDisable();
    }

    public static void onInitialize() {
        UltraHardcore.onInitialize();
    }

    public static UltraHardcorePlugin getInstance() {
        return instance;
    }
}
