package clutch.ultrahardcore.pluginload.biome;

import clutch.ultrahardcore.lang.LanguageManager;
import clutch.ultrahardcore.misc.UHCUtils;
import sun.misc.Unsafe;

import java.lang.reflect.*;
import java.util.HashSet;
import java.util.Set;

public class BiomeReplacer {

    private static final String BIOME_BASE = UHCUtils.CLASS_PACKAGE + "BiomeBase";
    private static final String MINECRAFT_KEY = UHCUtils.CLASS_PACKAGE + "MinecraftKey";
    private static final String REGISTRY_MATERIALS = UHCUtils.CLASS_PACKAGE + "RegistryMaterials";
    private static final String REGISTRY_ID = "REGISTRY_ID";
    private static final String BIOME_REGISTER_METHOD_NAME = "a";
    private static final String KEY_SET_METHOD_NAME = "keySet";
    private static final String GET_BIOME_METHOD_NAME = "get";
    private static final String REGISTRY_MATERIALS_BIOME_METHOD_NAME = "a";

    private static Status status;
    private static Method registerBiomeMethod;
    private static Object registryIDinstance;

    public static void init() {
        // Check if null
        if (status != null) return;
        status = Status.NOT_INITIALIZED;
        try {
            registryIDinstance = getRegistryCopiedWithMethodSetting();
            (registerBiomeMethod = (Method) methodSetting(searchForDeclaredMethod(Class.forName(BIOME_BASE),
                    BIOME_REGISTER_METHOD_NAME, int.class.getName(), String.class.getName(),
                    Class.forName(BIOME_BASE).getName()))).setAccessible(true);
            status = Status.INITIALIZED;
            LanguageManager.printFine("Biome Replacer: INITIALIZED");
        } catch (ReflectiveOperationException e) {
            status = Status.NOT_INITIALIZED;
            e.printStackTrace();
        }
    }

    public static void replaceBiome(Biomes biome, Biomes replacement) {
        if (status == Status.NOT_INITIALIZED) {
            LanguageManager.printError("Can't replace biome! Biome replacer is not initialized yet or had a problem!");
            status = Status.WARNED;
            return;
        }
        try {
            registerBiomeMethod.invoke(null, biome.getBiomeID(), biome.getBiomeName(),
                    methodSetting(getBiomeFromID(replacement.getBiomeName())));
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }
    }

    private static Object getBiomeFromID(String biomeName) throws ReflectiveOperationException {
        Object minecraftKeyOBJ = Class.forName(MINECRAFT_KEY).getConstructor(String.class).newInstance(biomeName);
        return getBiomeFromKey(registryIDinstance, minecraftKeyOBJ);
    }

    private static Object getBiomeFromKey(Object obj, Object key) throws ReflectiveOperationException {
        return searchForDeclaredMethod(obj.getClass(), GET_BIOME_METHOD_NAME, Object.class.getName()).invoke(obj, key);
    }

    private static Object getRegistryCopiedWithMethodSetting() throws ReflectiveOperationException {
        // Registry
        Object REGISTRY = Class.forName(BIOME_BASE).getField(REGISTRY_ID).get(null);
        // Keys
        Object keyObj = REGISTRY.getClass().getMethod(KEY_SET_METHOD_NAME).invoke(REGISTRY);
        Set<?> keys = keyObj instanceof Set ? (Set<?>) keyObj : new HashSet<>();
        // New registry materials
        Object newRegistryMaterials = Class.forName(REGISTRY_MATERIALS).newInstance();
        // Loop through keys
        for (Object key : keys) {
            newRegistryMaterials.getClass().getMethod(REGISTRY_MATERIALS_BIOME_METHOD_NAME, Object.class, Object.class)
                    .invoke(newRegistryMaterials, key, methodSetting(getBiomeFromKey(REGISTRY, key)));
        }
        return newRegistryMaterials;
    }

    private static Object methodSetting(Object o) throws ReflectiveOperationException {
        Field declaredField = Unsafe.class.getDeclaredField("theUnsafe");
        declaredField.setAccessible(true);
        Object allocateInstance = ((Unsafe) declaredField.get(null)).allocateInstance(o.getClass());
        for (Class<?> clazz = o.getClass(); !clazz.equals(Object.class); clazz = clazz.getSuperclass()) {
            Field[] declaredFields = clazz.getDeclaredFields();
            for (Field field : declaredFields) {
                if (!Modifier.isStatic(field.getModifiers())) {
                    field.setAccessible(true);
                    field.set(allocateInstance, field.get(o));
                }
            }
        }
        return allocateInstance;
    }

    private static Method searchForDeclaredMethod(Class classs, String methodName, String... paramTypeNames) {
        Method method = null;
        // Loop through methods
        for (Method foundMethod : classs.getDeclaredMethods()) {
            // Return the method if already found
            if (method != null) return method;
            // Check if method names are the same if not continue
            if (!foundMethod.getName().equals(methodName)) continue;
            // Get parameters for method
            Parameter[] params = foundMethod.getParameters();
            // Check if parameters is null if so continue
            if (params == null) continue;
            // Check if parameter array length is the same
            if (params.length != paramTypeNames.length) continue;
            // Loop through parameters
            for (int i = 0; i < params.length; i++) {
                // Check if names are the same if they are set the return method to that if not set to null
                if (params[i].getType().getName().equals(paramTypeNames[i])) {
                    method = foundMethod;
                } else {
                    method = null;
                }
            }
        }
        return method;
    }

    public enum Status {
        NOT_INITIALIZED,
        INITIALIZED,
        WARNED
    }
}
