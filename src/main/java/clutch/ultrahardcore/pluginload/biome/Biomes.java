package clutch.ultrahardcore.pluginload.biome;

public enum Biomes {

    ocean(0, "ocean"),
    plains(1, "plains"),
    desert(2, "desert"),
    extreme_hills(3, "extreme_hills"),
    forest(4, "forest"),
    taiga(5, "taiga"),
    swampland(6, "swampland"),
    river(7, "river"),
    hell(8, "hell"),
    sky(9, "sky"),
    frozen_ocean(10, "frozen_ocean"),
    frozen_river(11, "frozen_river"),
    ice_flats(12, "ice_flats"),
    ice_mountains(13, "ice_mountains"),
    mushroom_island(14, "mushroom_island"),
    mushroom_island_shore(15, "mushroom_island_shore"),
    beaches(16, "beaches"),
    desert_hills(17, "desert_hills"),
    forest_hills(18, "forest_hills"),
    taiga_hills(19, "taiga_hills"),
    smaller_extreme_hills(20, "smaller_extreme_hills"),
    jungle(21, "jungle"),
    jungle_hills(22, "jungle_hills"),
    jungle_edge(23, "jungle_edge"),
    deep_ocean(24, "deep_ocean"),
    stone_beach(25, "stone_beach"),
    cold_beach(26, "cold_beach"),
    birch_forest(27, "birch_forest"),
    birch_forest_hills(28, "birch_forest_hills"),
    roofed_forest(29, "roofed_forest"),
    taiga_cold(30, "taiga_cold"),
    taiga_cold_hills(31, "taiga_cold_hills"),
    redwood_taiga(32, "redwood_taiga"),
    redwood_taiga_hills(33, "redwood_taiga_hills"),
    extreme_hills_with_trees(34, "extreme_hills_with_trees"),
    savanna(35, "savanna"),
    savanna_rock(36, "savanna_rock"),
    mesa(37, "mesa"),
    mesa_rock(38, "mesa_rock"),
    mesa_clear_rock(39, "mesa_clear_rock"),
    mutated_plains(129, "mutated_plains"),
    mutated_desert(130, "mutated_desert"),
    mutated_extreme_hills(131, "mutated_extreme_hills"),
    mutated_forest(132, "mutated_forest"),
    mutated_taiga(133, "mutated_taiga"),
    mutated_swampland(134, "mutated_swampland"),
    mutated_ice_flats(140, "mutated_ice_flats"),
    mutated_jungle(149, "mutated_jungle"),
    mutated_jungle_edge(151, "mutated_jungle_edge"),
    mutated_birch_forest(155, "mutated_birch_forest"),
    mutated_birch_forest_hills(156, "mutated_birch_forest_hills"),
    mutated_roofed_forest(157, "mutated_roofed_forest"),
    mutated_taiga_cold(158, "mutated_taiga_cold"),
    mutated_redwood_taiga(160, "mutated_redwood_taiga"),
    mutated_redwood_taiga_hills(161, "mutated_redwood_taiga_hills"),
    mutated_extreme_hills_with_trees(162, "mutated_extreme_hills_with_trees"),
    mutated_savanna(163, "mutated_savanna"),
    mutated_savanna_rock(164, "mutated_savanna_rock"),
    mutated_mesa(165, "mutated_mesa"),
    mutated_mesa_rock(166, "mutated_mesa_rock"),
    mutated_mesa_clear_rock(167, "mutated_mesa_clear_rock");

    private int biomeID;
    private String biomeName;

    Biomes(Integer biomeID, String biomeName) {
        this.biomeID = biomeID;
        this.biomeName = biomeName;
    }

    public int getBiomeID() {
        return biomeID;
    }

    public String getBiomeName() {
        return biomeName;
    }
}
