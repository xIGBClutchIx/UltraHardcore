package clutch.ultrahardcore.inv;

import clutch.ultrahardcore.misc.UHCUtils;
import clutch.ultrahardcore.misc.Constants;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;
import java.util.UUID;

public class SpectatorInventoryManager {

    private static final int PLAYER_INVENTORY_SIZE = (9 * 3);
    private static final int INVENTORY_SIZE = (9 * 5);

    private static ArrayList<SpectatorInventory> viewers;

    // TODO INVENTORY NAME CHECKS
    // TODO OPEN AND CLOSE INVENTORY - EVENTS

    public static void init() {
        viewers = new ArrayList<>();
    }

    public static void openInventory(Player viewer, Player player) {
        String inventoryName = Constants.MAIN_COLOR + player.getDisplayName() + Constants.INVENTORY;
        Inventory inventory = Bukkit.createInventory(null, INVENTORY_SIZE, inventoryName);
        // Open
        viewer.openInventory(inventory);
        // Put
        if (getSpectatorInventory(player) != null) {
            addViewer(viewer, player);
        } else {
            addViewerToList(viewer, player);
        }
        // Armor
        updateArmor(player);
        // Active Potion Effects
        updateActiveEffect(player);
        // Hunger
        updateHunger(player);
        // Health
        updateHealth(player);
        // Content
        updateContents(player);
    }

    public static void closeInventory(Player viewer, Player player) {
        // TODO Close checks
        removeViewer(viewer, player);
    }

    /** UPDATE METHODS */

    public static void updateActiveEffect(Player player) {
        // Get inventory viewer for player
        SpectatorInventory playerInvViewer = getSpectatorInventory(player);
        // Return if inventory viewer is null
        if (playerInvViewer == null) return;
        // Loop through viewers
        for (UUID viewer : playerInvViewer.getViewers()) {
            // Get bukkit player from viewer uuid
            Player playerViewer = Bukkit.getPlayer(viewer);
            // Checks
            if (!checkForUpdate(player, playerViewer)) return;
            // Set Item
            playerViewer.getOpenInventory().getTopInventory().setItem(6, getActiveEffects(player));
        }
    }

    public static void updateHunger(Player player) {
        // Get inventory viewer for player
        SpectatorInventory playerInvViewer = getSpectatorInventory(player);
        // Return if inventory viewer is null
        if (playerInvViewer == null) return;
        // Loop through viewers
        for (UUID viewer : playerInvViewer.getViewers()) {
            // Get bukkit player from viewer uuid
            Player playerViewer = Bukkit.getPlayer(viewer);
            // Checks
            if (!checkForUpdate(player, playerViewer)) return;
            // Set Item
            playerViewer.getOpenInventory().getTopInventory().setItem(7, getHunger(player));
        }
    }

    public static void updateHealth(Player player) {
        // Get inventory viewer for player
        SpectatorInventory playerInvViewer = getSpectatorInventory(player);
        // Return if inventory viewer is null
        if (playerInvViewer == null) return;
        // Loop through viewers
        for (UUID viewer : playerInvViewer.getViewers()) {
            // Get bukkit player from viewer uuid
            Player playerViewer = Bukkit.getPlayer(viewer);
            // Checks
            if (!checkForUpdate(player, playerViewer)) return;
            // Set Item
            playerViewer.getOpenInventory().getTopInventory().setItem(8, getHealth(player));
        }
    }

    public static void updateContents(Player player) {
        // Get inventory viewer for player
        SpectatorInventory playerInvViewer = getSpectatorInventory(player);
        // Return if inventory viewer is null
        if (playerInvViewer == null) return;
        // Loop through viewers
        for (UUID viewer : playerInvViewer.getViewers()) {
            // Get bukkit player from viewer uuid
            Player playerViewer = Bukkit.getPlayer(viewer);
            // Checks
            if (!checkForUpdate(player, playerViewer)) return;
            // Inventories
            Inventory playerInv = playerViewer.getInventory();
            // Set inventory items
            for (int i = 9; i < PLAYER_INVENTORY_SIZE; i++) {
                playerViewer.getOpenInventory().getTopInventory().setItem(i, playerInv.getItem(i));
            }
            // Set inventory hotbar
            for (int i = INVENTORY_SIZE - 9; i < INVENTORY_SIZE; i++) {
                playerViewer.getOpenInventory().getTopInventory().setItem(i, playerInv.getItem(i - (INVENTORY_SIZE - 9)));
            }
        }
    }

    public static void updateArmor(Player player) {
        // Get inventory viewer for player
        SpectatorInventory playerInvViewer = getSpectatorInventory(player);
        // Return if inventory viewer is null
        if (playerInvViewer == null) return;
        // Loop through viewers
        for (UUID viewer : playerInvViewer.getViewers()) {
            Player playerViewer = Bukkit.getPlayer(viewer);
            // Checks
            if (!checkForUpdate(player, playerViewer)) return;
            // Inventories
            PlayerInventory playerInv = playerViewer.getInventory();
            Inventory playerViewerInv = playerViewer.getOpenInventory().getTopInventory();
            // Helmet
            if (playerInv.getHelmet() != null) playerViewerInv.setItem(0, playerInv.getHelmet());
            // Chestplate
            if (playerInv.getChestplate() != null) playerViewerInv.setItem(1, playerInv.getChestplate());
            // Leggings
            if (playerInv.getLeggings() != null) playerViewerInv.setItem(2, playerInv.getLeggings());
            // Boots
            if (playerInv.getBoots() != null) playerViewerInv.setItem(3, playerInv.getBoots());
        }
    }

    public static boolean checkForUpdate(Player player, Player viewer) {
        // Check player. Don't update if not on
        if (player == null || !player.isOnline()) return false;
        // Check viewer. Can't update if not on
        if (viewer == null || !viewer.isOnline()) return false;
        // Return if not opened
        if (viewer.getOpenInventory().getTopInventory() == null) return false;
        if (viewer.getOpenInventory().getTopInventory().getSize() != INVENTORY_SIZE) return false;
        // Double check if is player inventory
        return isPlayerInventory(viewer.getOpenInventory().getTopInventory());
    }

    /** ITEMSTACK METHODS */

    private static ItemStack getActiveEffects(Player player) {
        // Itemstack
        ItemStack itemStack = new Potion(PotionType.WATER).toItemStack(1);
        // Item meta and display name
        ItemMeta potionMeta = itemStack.getItemMeta();
        potionMeta.setDisplayName(Constants.POTION_EFFECTS);
        // Lore
        ArrayList<String> lore = new ArrayList<>();
        // If no effects then put a none active notice lore message
        if (player.getActivePotionEffects().isEmpty()) lore.add(Constants.NO_ACTIVE_EFFECT);
        // Loop through effects and add to lore
        for (PotionEffect potionEffect : player.getActivePotionEffects()) {
            // Name
            String name = Constants.EFFECT_MAIN_COLOR + UHCUtils.translatePotionEffect(potionEffect);
            // Amplifier
            if (potionEffect.getAmplifier() >= 1) {
                name = name + " " + UHCUtils.romanNumerals(potionEffect.getAmplifier() + 1);
            }
            // Separator
            name = name + Constants.SEPARATOR_COLOR + Constants.SEPARATOR + Constants.EFFECT_SECONDARY_COLOR;
            // Time
            name = name + (potionEffect.getDuration() / 20) + " sec";
            lore.add(name);
        }
        // Set lore and meta
        potionMeta.setLore(lore);
        itemStack.setItemMeta(potionMeta);
        // Return
        return itemStack;
    }

    private static ItemStack getHunger(Player player) {
        // Itemstack
        ItemStack itemStack = new ItemStack(Material.COOKED_CHICKEN, Math.max(1, player.getFoodLevel()));
        // ItemMeta
        ItemMeta healthPotionMeta = itemStack.getItemMeta();
        // Display name
        healthPotionMeta.setDisplayName(Constants.HUNGER);
        // Set meta
        itemStack.setItemMeta(healthPotionMeta);
        // Return
        return itemStack;
    }

    private static ItemStack getHealth(Player player) {
        // Itemstack
        ItemStack itemStack = new Potion(PotionType.INSTANT_HEAL).toItemStack(Math.max(1, (int) player.getHealth()));
        // ItemMeta
        ItemMeta healthPotionMeta = itemStack.getItemMeta();
        // Display name
        healthPotionMeta.setDisplayName(Constants.HEALTH);
        // Set meta
        itemStack.setItemMeta(healthPotionMeta);
        // Return
        return itemStack;
    }

    /** VIEWER METHODS */

    private static void addViewer(Player viewer, Player player) {
        // Get viewer
        SpectatorInventory spectatorInventory = getSpectatorInventory(player);
        // Return if does not exist
        if (spectatorInventory == null) return;
        // Added viewer to inventory viewer info
        spectatorInventory.addViewer(viewer);
    }

    private static void removeViewer(Player viewer, Player player) {
        // Get viewer
        SpectatorInventory spectatorInventory = getSpectatorInventory(player);
        // Return if does not exist
        if (spectatorInventory == null) return;
        // If final viewer is remove just remove it from tracking
        if (spectatorInventory.removeViewer(viewer)) {
            viewers.remove(spectatorInventory);
        }
    }

    private static SpectatorInventory addViewerToList(Player viewer, Player player) {
        // Create viewer and set player getting viewed
        SpectatorInventory spectatorInventory = new SpectatorInventory(player);
        // Added viewer to inventory viewer info
        spectatorInventory.addViewer(viewer);
        // Add to list
        viewers.add(spectatorInventory);
        // Return
        return getSpectatorInventory(player);
    }

    private static SpectatorInventory getSpectatorInventory(Player player) {
        // Try to find viewer from list
        for (SpectatorInventory viewer : viewers) {
            // Check if uuids are the same
            if (viewer.getPlayer().equals(player.getUniqueId())) {
                // Return
                return viewer;
            }
        }
        // Return null if nothing is found
        return null;
    }

    private static String getPlayerInventoryName(Inventory inventory) {
        // Inventory name
        String name = inventory.getName();
        // If in player inventory then strip colors and trim end. If not just return nothing.
        if (isPlayerInventory(inventory)) {
            return UHCUtils.stripChatColor(name.replaceAll(Constants.INVENTORY, ""));
        } else {
            return "";
        }
    }

    private static boolean isPlayerInventory(Inventory inventory) {
        // Check if ends with Inv
        return inventory.getName().endsWith(Constants.INVENTORY);
    }
}
