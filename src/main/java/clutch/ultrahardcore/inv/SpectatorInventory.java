package clutch.ultrahardcore.inv;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class SpectatorInventory {

    private UUID player;
    private ArrayList<UUID> viewers = new ArrayList<>();

    public SpectatorInventory(Player player) {
        this.player = player.getUniqueId();
    }

    public void addViewer(Player viewer) {
        // Add viewer
        viewers.add(viewer.getUniqueId());
    }

    public boolean removeViewer(Player viewer) {
        // Remove
        viewers.remove(viewer.getUniqueId());
        // Return true if viewer list is empty
        return viewers.isEmpty();
    }

    public ArrayList<UUID> getViewers() {
        return viewers;
    }

    public UUID getPlayer() {
        return player;
    }
}
