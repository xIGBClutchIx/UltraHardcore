package clutch.ultrahardcore;

import clutch.ultrahardcore.commands.UHCCommand;
import clutch.ultrahardcore.game.Game;
import clutch.ultrahardcore.inv.SpectatorInventoryManager;
import clutch.ultrahardcore.lang.LanguageManager;
import clutch.ultrahardcore.misc.Constants;
import clutch.ultrahardcore.misc.UHCUtils;
import clutch.ultrahardcore.pluginload.PluginLoadManger;
import org.bukkit.event.Listener;

public class UltraHardcore implements Listener {

    /** Loads before world generation */
    public static void onEnable() {
        // TODO REMOVE TEMP
        UHCUtils.registerListener(new TempListener());
    }

    /** Loads after world generation */
    public static void onInitialize() {
        // Return if already loaded
        if (PluginLoadManger.isLoaded()) return;
        // TODO REMOVE TEMP
        TempListener.initSpectateTimer();
        // Init managers
        LanguageManager.init();
        SpectatorInventoryManager.init();
        // Done loading
        PluginLoadManger.loaded();
        // Init game
        Game.init();
        // Register command
        UHCUtils.registerCommand(Constants.SHORTENED_NAME, new UHCCommand());
    }

    /** Loads on disabling of plugin */
    public static void onDisable() {
    }
}
