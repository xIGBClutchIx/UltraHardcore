package clutch.ultrahardcore.game;

import clutch.ultrahardcore.misc.Constants;
import org.bukkit.ChatColor;

public enum GameState {

    WAITING(Constants.INFO_COLOR),
    STARTING(Constants.FINE_COLOR),
    STARTED(Constants.DONE_COLOR),
    ENDING(Constants.ERROR_COLOR);

    ChatColor color;

    GameState(ChatColor color) {
        this.color = color;
    }

    public ChatColor getColor() {
        return color;
    }
}
