package clutch.ultrahardcore.game.runnables;

import clutch.ultrahardcore.game.SettingsManager;
import clutch.ultrahardcore.lang.LanguageManager;

public class PlayerDamageRunnable implements Runnable {

    private int loadCycle = 7;

    @Override
    public void run() {
        if (loadCycle == 0) {
            return;
        }
        loadCycle--;
        if (loadCycle != 0) {
            LanguageManager.broadcast("Enabling player damage in " + (loadCycle * 5));
        } else {
            LanguageManager.broadcast("Enabled player damage");
            SettingsManager.setShouldPlayerTakeDamage(true);
        }
    }
}
