package clutch.ultrahardcore.game.runnables;

import clutch.ultrahardcore.game.Game;
import clutch.ultrahardcore.game.GameState;
import clutch.ultrahardcore.game.teams.TeamManager;
import clutch.ultrahardcore.lang.LanguageManager;
import clutch.ultrahardcore.misc.UHCUtils;

public class GameTimerRunnable implements Runnable {

    // TODO Move to config
    private int NEEDED_PLAYERS = 1;
    private int loadCycle = 6;

    @Override
    public void run() {
        GameState gameState = Game.getState();
        if (gameState == GameState.WAITING) {
            if (Game.getNumberOfPlayersOnline() >= NEEDED_PLAYERS) {
                Game.setState(GameState.STARTING);
            } else {
                // LanguageManager.broadcast("Waiting on more players!");
            }
        } else if (gameState == GameState.STARTING) {
            if (Game.getNumberOfPlayersOnline() < NEEDED_PLAYERS) {
                Game.setState(GameState.WAITING);
                loadCycle = 6;
                LanguageManager.broadcast("Need more players!");
                return;
            }
            if (loadCycle == 0) {
                startGame();
                return;
            }
            LanguageManager.broadcast("Game starting in " + (loadCycle * 5));
            loadCycle--;
        }
    }

    public void startGame() {
        Game.setState(GameState.STARTED);
        TeamManager.teleportAllTeams();
        LanguageManager.broadcast("Game started");
        UHCUtils.runTaskTimer(new PlayerDamageRunnable(), 5, 5);
    }
}
