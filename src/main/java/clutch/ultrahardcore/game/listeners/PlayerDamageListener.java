package clutch.ultrahardcore.game.listeners;

import clutch.ultrahardcore.game.SettingsManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class PlayerDamageListener implements Listener {

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent event) {
        if (SettingsManager.shouldPlayerTakeDamage()) return;
        if (event.getEntity() instanceof Player) {
            event.setCancelled(true);
        }
    }
}
