package clutch.ultrahardcore.game.listeners.settings;

import clutch.ultrahardcore.game.SettingsManager;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

public class NoUncookedFoodListener implements Listener {

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        if (!SettingsManager.shouldRemoveUncookedFood()) return;
        ItemStack itemStack = event.getItem().getItemStack();
        switch (itemStack.getType()) {
            case PORK:
                itemStack.setType(Material.GRILLED_PORK);
                break;
            case RAW_BEEF:
                itemStack.setType(Material.COOKED_BEEF);
                break;
            case MUTTON:
                itemStack.setType(Material.COOKED_MUTTON);
                break;
            case RAW_FISH:
                itemStack.setType(Material.COOKED_FISH);
                break;
            case RAW_CHICKEN:
                itemStack.setType(Material.COOKED_CHICKEN);
                break;
            case RABBIT:
                itemStack.setType(Material.COOKED_RABBIT);
                break;
        }
    }
}
