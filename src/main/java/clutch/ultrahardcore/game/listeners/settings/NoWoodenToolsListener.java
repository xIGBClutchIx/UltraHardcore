package clutch.ultrahardcore.game.listeners.settings;

import clutch.ultrahardcore.game.SettingsManager;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.ItemStack;

public class NoWoodenToolsListener implements Listener {

    @EventHandler
    public void onCraftItem(CraftItemEvent event) {
        if (!SettingsManager.shouldRemoveWoodenTools()) return;
        ItemStack itemStack = event.getCurrentItem();
        switch (itemStack.getType()) {
            case WOOD_AXE:
                itemStack.setType(Material.STONE_AXE);
                break;
            case WOOD_HOE:
                itemStack.setType(Material.STONE_HOE);
                break;
            case WOOD_PICKAXE:
                itemStack.setType(Material.STONE_PICKAXE);
                break;
            case WOOD_SPADE:
                itemStack.setType(Material.STONE_SPADE);
                break;
            case WOOD_SWORD:
                itemStack.setType(Material.STONE_SWORD);
                break;
        }
    }
}
