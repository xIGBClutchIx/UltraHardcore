package clutch.ultrahardcore.game;

public class SettingsManager {

    private static boolean playerDamage = false;
    private static boolean removedWoodenTools = false;
    private static boolean removeUncookedFood = false;

    public static void init() {
    }

    public static boolean shouldPlayerTakeDamage() {
        return playerDamage;
    }

    public static void setShouldPlayerTakeDamage(boolean enabled) {
        playerDamage = enabled;
    }

    public static boolean shouldRemoveWoodenTools() {
        return removedWoodenTools;
    }

    public static void setRemovedWoodenTools(boolean enabled) {
        removedWoodenTools = enabled;
    }

    public static boolean shouldRemoveUncookedFood() {
        return removeUncookedFood;
    }

    public static void setRemoveUncookedFood(boolean enabled) {
        removeUncookedFood = enabled;
    }
}
