package clutch.ultrahardcore.game;

import clutch.ultrahardcore.game.listeners.settings.NoUncookedFoodListener;
import clutch.ultrahardcore.game.listeners.settings.NoWoodenToolsListener;
import clutch.ultrahardcore.game.listeners.PlayerDamageListener;
import clutch.ultrahardcore.game.runnables.GameTimerRunnable;
import clutch.ultrahardcore.game.maps.MapManager;
import clutch.ultrahardcore.game.teams.TeamManager;
import clutch.ultrahardcore.lang.LanguageManager;
import clutch.ultrahardcore.misc.UHCUtils;
import org.bukkit.entity.Player;

public class Game {

    /* State */
    private static GameState state = null;

    public static void init() {
        // Check if game is initialized
        if (state != null) return;
        // Set initial state
        setState(GameState.WAITING);
        // Init managers
        SettingsManager.init();
        MapManager.init();
        TeamManager.init();
        // Register listeners
        UHCUtils.registerListener(new NoWoodenToolsListener());
        UHCUtils.registerListener(new NoUncookedFoodListener());
        UHCUtils.registerListener(new PlayerDamageListener());
        // Start game timer
        UHCUtils.runTaskTimer(new GameTimerRunnable(), 5, 5);
    }

    public static int getNumberOfPlayersOnline() {
        return UHCUtils.getOnlinePlayers().size();
    }

    public static int getNumberOfPlayersOnTeams() {
        int players = 0;
        for (Player player : UHCUtils.getOnlinePlayers()) {
            if (TeamManager.isOnTeam(player)) {
                players++;
            }
        }
        return players;
    }

    /** State */
    public static GameState getState() {
        return state;
    }

    public static void setState(GameState newState) {
        // Check if null
        if (newState == null) return;
        // Set state and print info
        state = newState;
        LanguageManager.printColor(newState.getColor(), "Game Status: " + newState.name());
    }
}
