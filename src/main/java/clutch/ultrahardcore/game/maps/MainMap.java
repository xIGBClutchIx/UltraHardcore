package clutch.ultrahardcore.game.maps;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityRegainHealthEvent;

public class MainMap extends Map {

    /** ULTRA HARDCORE HEALTH REGEN */
    @EventHandler
    public void onEntityRegainHealth(EntityRegainHealthEvent event) {
        if (event.getEntity() instanceof Player) {
            if (event.getRegainReason() == EntityRegainHealthEvent.RegainReason.SATIATED) {
                event.setCancelled(true);
            }
        }
    }

    @Override
    public String[] getNames() {
        return new String[]{"world", "world_nether", "world_the_end"};
    }

    @Override
    public boolean deleteWorldOnEnd() {
        return true;
    }
}
