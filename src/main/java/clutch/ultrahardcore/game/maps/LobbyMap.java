package clutch.ultrahardcore.game.maps;

public class LobbyMap extends Map {

    @Override
    public String[] getNames() {
        return new String[]{"world_lobby"};
    }

    @Override
    public boolean deleteWorldOnEnd() {
        return false;
    }
}
