package clutch.ultrahardcore.game.maps;

import clutch.ultrahardcore.misc.UHCUtils;
import org.bukkit.event.Listener;

public abstract class Map implements Listener {

    public void init() {
        // Register events for all map classes
        UHCUtils.registerListener(this);
        // Loop through map names
        for (String mapName : getNames()) {
            // Create world. If world already exists it should just load
            UHCUtils.createUHCWorld(mapName);
        }
    }

    public abstract String[] getNames();
    public abstract boolean deleteWorldOnEnd();
}
