package clutch.ultrahardcore.game.maps;

import clutch.ultrahardcore.game.Game;
import clutch.ultrahardcore.game.GameState;

public class MapManager {

    private static MainMap mainMap = new MainMap();
    private static LobbyMap lobbyMap = new LobbyMap();

    public static void init() {
        // Init Maps
        mainMap.init();
        lobbyMap.init();
    }

    public static Map getCurrentMap() {
        GameState state = Game.getState();
        if (state == GameState.WAITING || state == GameState.STARTING) {
            return lobbyMap;
        } else {
            return mainMap;
        }
    }
}
