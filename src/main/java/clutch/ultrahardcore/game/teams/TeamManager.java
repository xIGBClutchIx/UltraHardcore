package clutch.ultrahardcore.game.teams;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class TeamManager {

    private static ArrayList<Team> teams;

    public static void init() {
        teams = new ArrayList<>();
    }

    public static boolean addPlayer(Player player, String teamName) {
        // Add player with their uuid to the team
        return addPlayer(player.getUniqueId(), teamName);
    }

    public static boolean addPlayer(UUID player, String teamName) {
        // Check if team exists and player is on a team
        if (doesTeamExist(teamName) && !isOnTeam(player)) {
            // Add player to team
            getTeam(teamName).addPlayer(player);
            // Return true that we added
            return true;
        }
        // Return false that we did not add
        return false;
    }

    public static boolean removePlayer(Player player) {
        // Remove player with their uuid
        return removePlayer(player.getUniqueId());
    }

    public static boolean removePlayer(UUID player) {
        // Check if player is on a team
        if (isOnTeam(player)) {
            // Get team for player and remove the player
            getTeam(player).removePlayer(player);
            // Return true that we removed
            return true;
        }
        // Return false that we did not remove
        return false;
    }

    public static Team addTeam(String teamName, ChatColor color) {
        // Check if team does not exist
        if (!doesTeamExist(teamName)) {
            // Create team with name and color
            Team team = new Team(teamName, color);
            // Add team to list
            teams.add(team);
            // Return the team
            return team;
        } else {
            // Return team if it exists
            return getTeam(teamName);
        }
    }

    public static boolean removeTeam(String teamName) {
        // Check if team exist
        if (doesTeamExist(teamName)) {
            // Get team and remove it from list
            teams.remove(getTeam(teamName));
            // Return true that we removed
            return true;
        }
        // Return false that we did not remove
        return false;
    }

    public static boolean isOnTeam(Player player) {
        // Return true if player with uuid is on a team
        return isOnTeam(player.getUniqueId());
    }

    public static boolean isOnTeam(UUID uuid) {
        // Return true if uuid is on a team
        return getTeam(uuid) != null;
    }

    public static Team getTeam(Player player) {
        // Return the team if player with uuid is on a team
        return getTeam(player.getUniqueId());
    }

    public static Team getTeam(UUID uuid) {
        for (Team team : teams) {
            // Check if uuid is on team
            if (team.isPlayerOnTeam(uuid)) {
                // Return the team if so
                return team;
            }
        }
        // Return null if no team for that player exists
        return null;
    }

    public static boolean doesTeamExist(String teamName) {
        // Return true if team is not null based on name
        return getTeam(teamName) != null;
    }

    public static Team getTeam(String teamName) {
        for (Team team : teams) {
            // Check if team names are the same
            if (team.getName().equalsIgnoreCase(teamName)) {
                // Return the team if so
                return team;
            }
        }
        // Return null if no team for that name exists
        return null;
    }

    public static void teleportAllTeams() {
        teams.forEach(Team::teleportTeam);
    }
}
