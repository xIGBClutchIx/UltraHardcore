package clutch.ultrahardcore.game.teams;

import clutch.ultrahardcore.misc.Constants;
import clutch.ultrahardcore.misc.UHCUtils;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.UUID;

public class Team {

    private ArrayList<UUID> players = new ArrayList<>();
    private String name;
    private ChatColor color;
    private Location teleportLocation;

    public Team(String name, ChatColor color) {
        this.name = name == null ? "" : name;
        this.color = color == null ? Constants.NO_TEAM_COLOR : color;
        // TODO Switch to main world thing
        World world = UHCUtils.getWorlds().get(0);
        this.teleportLocation = new Location(world, 0, world.getHighestBlockYAt(0, 0) + 3, 0);
    }

    public void sendMessageToTeam(String msg) {
        for (UUID player : players) {
            // Get bukkit player from uuid
            Player bukkitPlayer = UHCUtils.getPlayer(player);
            // Check player
            if (bukkitPlayer == null || !bukkitPlayer.isOnline()) return;
            // Message player
            bukkitPlayer.sendMessage(msg);
        }
    }

    protected void addPlayer(UUID uuid) {
        // Add player
        players.add(uuid);
    }

    protected void removePlayer(UUID uuid) {
        // Remove player
        players.remove(uuid);
    }

    protected boolean isPlayerOnTeam(UUID uuid) {
        return players.contains(uuid);
    }

    public void teleportTeam() {
        // Clear and reset all the things
        for (UUID playerUUID : players) {
            Player player = UHCUtils.getPlayer(playerUUID);
            player.getPlayer().setGameMode(GameMode.SURVIVAL);
            player.getPlayer().setItemOnCursor(null);
            player.getPlayer().getInventory().clear();
            player.getPlayer().getInventory().setArmorContents(new ItemStack[4]);
            player.getPlayer().getInventory().setHeldItemSlot(0);
            player.getPlayer().getInventory().setItemInMainHand(null);
            player.getPlayer().getInventory().setItemInOffHand(null);
            for (PotionEffect potionEffect : player.getPlayer().getActivePotionEffects()) {
                player.getPlayer().removePotionEffect(potionEffect.getType());
            }
            player.getPlayer().setFoodLevel(20);
            player.getPlayer().setExhaustion(0);
            player.getPlayer().setSaturation(20);
            player.getPlayer().setHealth(20);
            player.getPlayer().setFallDistance(0);
            player.getPlayer().setFireTicks(0);
            player.getPlayer().resetMaxHealth();
            player.getPlayer().setExp(0);
            player.getPlayer().setLevel(0);
            player.getPlayer().setFlying(false);
            player.getPlayer().setSneaking(false);
            player.getPlayer().setSprinting(false);
            player.getPlayer().updateInventory();
            // Teleport
            Location location = teleportLocation;
            // Offset by 0.5 on x
            if (location.getBlockX() >= 1) {
                location.add(0.5, 0, 0);
            } else {
                location.subtract(0.5, 0, 0);
            }
            // Offset by 0.5 on z
            if (location.getBlockZ() >= 1) {
                location.add(0, 0, 0.5);
            } else {
                location.subtract(0, 0, 0.5);
            }
            player.teleport(location.add(0, 10 ,0));
        }
    }

    public ArrayList<UUID> getPlayers() {
        return players;
    }

    public String getName() {
        return name;
    }

    public ChatColor getColor() {
        return color;
    }

    public Location getTeleportLocation() {
        return teleportLocation;
    }
}
