package clutch.ultrahardcore.misc;

import org.bukkit.ChatColor;

public class Constants {

    public static final String NAME = "UltraHardcore";
    public static final String SHORTENED_NAME = "UHC";

    public static final String SEPARATOR = " > ";

    public static final ChatColor MAIN_COLOR = ChatColor.DARK_AQUA;
    public static final ChatColor SEPARATOR_COLOR = ChatColor.DARK_GRAY;
    public static final ChatColor SECONDARY_COLOR = ChatColor.GRAY;

    public static final ChatColor FINE_COLOR = ChatColor.GREEN;
    public static final ChatColor DONE_COLOR = ChatColor.DARK_GREEN;
    public static final ChatColor INFO_COLOR = ChatColor.YELLOW;
    public static final ChatColor ERROR_COLOR = ChatColor.RED;

    /** Spectate Inventory */
    public static final String INVENTORY = " Inventory";

    public static final String POTION_EFFECTS = MAIN_COLOR + "Potion Effects";
    public static final String NO_ACTIVE_EFFECT = SEPARATOR_COLOR + "None active";

    public static final ChatColor EFFECT_MAIN_COLOR = ChatColor.DARK_GREEN;
    public static final ChatColor EFFECT_SECONDARY_COLOR = ChatColor.GREEN;

    public static final String HEALTH = MAIN_COLOR + "Health";
    public static final String HUNGER = MAIN_COLOR + "Hunger";

    /** Teams */
    public static final ChatColor[] TEAM_COLORS_ORDER = {ChatColor.BLUE, ChatColor.RED, ChatColor.DARK_GREEN,
                                                         ChatColor.GOLD, ChatColor.DARK_PURPLE, ChatColor.DARK_AQUA,
                                                         ChatColor.DARK_BLUE, ChatColor.DARK_RED, ChatColor.GREEN,
                                                         ChatColor.YELLOW, ChatColor.LIGHT_PURPLE, ChatColor.AQUA};

    public static final ChatColor NO_TEAM_COLOR = ChatColor.DARK_GRAY;

}
