package clutch.ultrahardcore.misc;

import clutch.ultrahardcore.pluginload.UltraHardcorePlugin;
import org.bukkit.*;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;

public class UHCUtils {

    /** NMS version and packages */
    public static final String SERVER_PACKAGE_NAME = Bukkit.getServer().getClass().getPackage().getName();
    public static final String SERVER_VERSION = SERVER_PACKAGE_NAME.substring(SERVER_PACKAGE_NAME.lastIndexOf(".") + 1);
    public static final String CLASS_PACKAGE = "net.minecraft.server." + SERVER_VERSION + ".";

    /** Chat Color Utils */
    public static String stripChatColor(String text) {
        return ChatColor.stripColor(text);
    }

    /** Bukkit Utils */
    public static void registerCommand(String name, CommandExecutor executor) {
        UltraHardcorePlugin.getInstance().getCommand(name).setExecutor(executor);
    }

    public static World createUHCWorld(String worldName) {
        // Create world with default world creator
        World world = Bukkit.createWorld(new WorldCreator(worldName));
        // Set difficulty
        world.setDifficulty(Difficulty.HARD);
        // Turn off natural regeneration. Should not matter because of the RegainHealthEvent cancel but just a backup.
        world.setGameRuleValue("naturalRegeneration", "false");
        return world;
    }

    public static Player getPlayer(UUID player) {
        return Bukkit.getPlayer(player);
    }

    public static List<World> getWorlds() {
        return Bukkit.getWorlds();
    }

    public static Collection<? extends Player> getOnlinePlayers() {
        return Bukkit.getOnlinePlayers();
    }

    public static int getNumberOfWorlds() {
        return Bukkit.getWorlds().size();
    }

    public static void broadcastMessage(String message) {
        Bukkit.broadcastMessage(message);
    }

    public static void consoleMessage(String message) {
        Bukkit.getConsoleSender().sendMessage(message);
    }

    public static void registerListener(Listener listener) {
        Bukkit.getServer().getPluginManager().registerEvents(listener, UltraHardcorePlugin.getInstance());
    }

    public static BukkitTask runTaskTimer(Runnable runnable, int delay, int time) {
        return Bukkit.getScheduler().runTaskTimer(UltraHardcorePlugin.getInstance(), runnable, delay * 20, time * 20);
    }

    public static BukkitTask runTaskTimerAsynchronously(Runnable runnable, int delay, int time) {
        return Bukkit.getScheduler().runTaskTimerAsynchronously(UltraHardcorePlugin.getInstance(), runnable,
                delay * 20, time * 20);
    }

    public static void cancelTask(int taskID) {
        Bukkit.getScheduler().cancelTask(taskID);
    }

    /** Potion effect translate */
    public static String translatePotionEffect(PotionEffect potionEffect) {
        switch (potionEffect.getType().getName()) {
            case "POISON":
                return "Poison";
            case "NIGHT_VISION":
                return "Night Vision";
            case "SLOW_DIGGING":
                return "Mining Fatigue";
            case "WITHER":
                return "Wither";
            case "INCREASE_DAMAGE":
                return "Strength";
            case "BLINDNESS":
                return "Blindness";
            case "WATER_BREATHING":
                return "Water Breathing";
            case "REGENERATION":
                return "Regeneration";
            case "ABSORPTION":
                return "Absorption";
            case "FAST_DIGGING":
                return "Haste";
            case "HEALTH_BOOST":
                return "Health Boost";
            case "HARM":
                return "Instant Damage";
            case "HEAL":
                return "Instant Health";
            case "JUMP":
                return "Jump Boost";
            case "SLOW":
                return "Slowness";
            case "WEAKNESS":
                return "Weakness";
            case "Speed":
                return "Speed";
            case "SATURATION":
                return "Saturation";
            case "DAMAGE_RESISTANCE":
                return "Resistance";
            case "INVISIBILITY":
                return "Invisibility";
            case "FIRE_RESISTANCE":
                return "Fire Resistance";
            case "CONFUSION":
                return "Nausea";
            case "HUNGER":
                return "Hunger";
            default:
                return "Unknown Effect";
        }
    }

    /** Misc Utils */
    public static String romanNumerals(int number) {
        LinkedHashMap<String, Integer> romanNumerals = new LinkedHashMap<>();
        romanNumerals.put("M", 1000);
        romanNumerals.put("CM", 900);
        romanNumerals.put("D", 500);
        romanNumerals.put("CD", 400);
        romanNumerals.put("C", 100);
        romanNumerals.put("XC", 90);
        romanNumerals.put("L", 50);
        romanNumerals.put("XL", 40);
        romanNumerals.put("X", 10);
        romanNumerals.put("IX", 9);
        romanNumerals.put("V", 5);
        romanNumerals.put("IV", 4);
        romanNumerals.put("I", 1);
        String endNumeral = "";
        for (Map.Entry<String, Integer> entry : romanNumerals.entrySet()) {
            int matches = number / entry.getValue();
            endNumeral += repeat(entry.getKey(), matches);
            number = number % entry.getValue();
        }
        return endNumeral;
    }

    public static String repeat(String key, int matches) {
        if (key == null) return null;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i <= matches; i++) {
            stringBuilder.append(key);
        }
        return stringBuilder.toString();
    }
}
