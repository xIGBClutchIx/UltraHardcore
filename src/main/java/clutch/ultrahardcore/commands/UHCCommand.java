package clutch.ultrahardcore.commands;

import clutch.ultrahardcore.inv.SpectatorInventoryManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class UHCCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            SpectatorInventoryManager.openInventory(player, player);
            return true;
        }
        return false;
    }
}
