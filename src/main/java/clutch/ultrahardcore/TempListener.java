package clutch.ultrahardcore;

import clutch.ultrahardcore.game.SettingsManager;
import clutch.ultrahardcore.game.teams.TeamManager;
import clutch.ultrahardcore.inv.SpectatorInventoryManager;
import clutch.ultrahardcore.misc.UHCUtils;
import clutch.ultrahardcore.misc.Constants;
import clutch.ultrahardcore.pluginload.biome.BiomeReplacer;
import clutch.ultrahardcore.pluginload.biome.Biomes;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class TempListener implements Listener {

    public TempListener() {
        BiomeReplacer.replaceBiome(Biomes.plains, Biomes.desert);
    }

    public static void initSpectateTimer() {
        UHCUtils.runTaskTimerAsynchronously(() -> {
            for (Player player : UHCUtils.getOnlinePlayers()) {
                // Armor
                SpectatorInventoryManager.updateArmor(player);
                // Active Potion Effects
                SpectatorInventoryManager.updateActiveEffect(player);
                // Hunger
                SpectatorInventoryManager.updateHunger(player);
                // Health
                SpectatorInventoryManager.updateHealth(player);
                // Content
                SpectatorInventoryManager.updateContents(player);
            }
        }, 0, 1);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        String teamName = "Test";
        // Add team and player
        TeamManager.addTeam(teamName, Constants.TEAM_COLORS_ORDER[0]);
        TeamManager.addPlayer(event.getPlayer(), teamName);
        // Settings
        SettingsManager.setRemovedWoodenTools(true);
        SettingsManager.setRemoveUncookedFood(true);
    }
}
