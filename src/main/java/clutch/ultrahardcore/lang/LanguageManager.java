package clutch.ultrahardcore.lang;

import clutch.ultrahardcore.misc.UHCUtils;
import clutch.ultrahardcore.misc.Constants;
import clutch.ultrahardcore.pluginload.UltraHardcorePlugin;
import org.bukkit.ChatColor;

public class LanguageManager {

    private static String messageFormat = Constants.MAIN_COLOR + "%s" +
            Constants.SEPARATOR_COLOR + Constants.SEPARATOR + Constants.SECONDARY_COLOR + "%s";

    public static void init() {
    }

    public static void broadcast(String message) {
        UHCUtils.broadcastMessage(shortNameFormat(message));
    }

    public static void printFine(String message) {
        printColor(Constants.FINE_COLOR, message);
    }

    public static void printInfo(String message) {
        printColor(Constants.INFO_COLOR, message);
    }

    public static void printError(String message) {
        printColor(Constants.ERROR_COLOR, message);
    }

    public static void printColor(ChatColor color, String message) {
        print(color + message);
    }

    public static void print(String message) {
        UHCUtils.consoleMessage(fullNameFormat(message));
    }

    public static String shortNameFormat(String message) {
        return formatMessage(Constants.SHORTENED_NAME, message);
    }

    public static String fullNameFormat(String message) {
        return formatMessage(Constants.NAME, message);
    }

    private static String formatMessage(String messagePrefix, String message) {
        return String.format(messageFormat, messagePrefix, message);
    }
}
